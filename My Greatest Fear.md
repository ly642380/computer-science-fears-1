# Computer-Science-1 - "My Greatest Fear.md"
README.md

My Greatest Fear

**BUGS**

While bugs have been on my mind for a while now, they've also been in my backyard. 
Finding bugs in your house, of course, is a tragedy in itself, but the fear that they'll crawl up your leg, up your arm, and the tingling sensation too.
It's too much, having a bug unexpectedly crawling up a limb. Especially unexpectedly, not knowing what's tickling your leg makes me want to slap it off.
The fright felt when that happens is tremendous, I might not like dark alleys too. 
The next level is when they fly into your ear, like, what can you do about that? You can't delicately pull it out while reacting as quickly as possible.
You know, if the bug wanted to, it could probably live in your ear for the rest of your life; have a family in there. **You know how terrifying that is?**
If I can't see it but feel it, it shall not live. If I can't see it or feel it, it shall reside on this planet.
Super exaggerated explaination of why I'm afraid of bugs. 
